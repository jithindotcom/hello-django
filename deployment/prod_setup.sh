export REST_API_DB_NAME=$DBName
export REST_API_DB_USER=$DBUser
export REST_API_DB_PASS=$DBPass
export REST_API_DB_HOST=$DBHost
export REST_API_DB_PORT=$DBPort
export AWS_ACCESS_KEY_ID=$AWSAccessKey
export AWS_SECRET_ACCESS_KEY=$AWSSecretKey
export AWS_STORAGE_BUCKET_NAME=$BucketName
export AWS_S3_ENCRYPTION=$Encryption
export AWS_S3_REGION_NAME=$RegionName
export AUTH_ENABLED=$Enabled
export OKTA_RESOURCE_SERVER_INTROSPECTION_URL=$ResourceURL
export OKTA_CLIENT_ID=$ClientID
export OKTA_SECRET_KEY=$SecretKey
export PATH=$PATH:$PostgresPath
export FRAMEWORK_TEMPLATE_PATH=templates
export TABLEAU_AUTH_URL=$TableauAuthUrl
yum update
yum install -y python36 python36-libs python36-devel python36-pip
pip install virtualenv awscli