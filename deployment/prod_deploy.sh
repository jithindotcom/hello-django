mkdir -p /opt/build/
cd /opt/build
git clone $URL
cd compliance_mapping_apis
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
aws s3 cp templates/ s3://$AWS_STORAGE_BUCKET_NAME/ --recursive
python manage.py makemigrations
python manage.py migrate
nohup python manage.py runserver 0.0.0.0:8000 > run.log &